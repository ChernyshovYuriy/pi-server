# Pi-Server

This is publisher of custom messages intended to run on Raspberry Pi hardware. It should collect all data from the external sensors, such as speed-, light-, water-sensors, etc, create custom message and send it via IPC mechanism.

The IPC mechanism is implemented by [LCM](https://lcm-proj.github.io/index.html).

Structure of message defined in **pidata_t.lcm** document. See [specification](https://lcm-proj.github.io/type_specification.html) document for available data types and [example](https://lcm-proj.github.io/tut_lcmgen.html) to get highlights of customization.

The name of channel used by LCM is **RaspberryPiData**

GUI is designed based on [Tkinter](https://en.wikipedia.org/wiki/Tkinter) framework.

Operating system is Linux only.