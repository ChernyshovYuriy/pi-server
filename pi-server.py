import _thread
import time
import tkinter
from tkinter import Button, Frame, Text
from tkinter.constants import BOTH, W, E, END
from tkinter.ttk import Style

import lcm

from pilcm import pidata_t

WIDTH, HEIGHT = 600, 400
CHANNEL_NAME = "RaspberryPiData"


class App(Frame):
    def __init__(self, parent, **kw):
        super().__init__(**kw)
        self.lc = None
        self.is_server_running = False
        self.is_init = False

        Frame.__init__(self, parent, background="#a3aea7")
        self.parent = parent
        self.parent.title("Data center")
        self.style = Style()
        self.style.theme_use("default")
        self.centre_window()
        self.pack(fill=BOTH, expand=1)

        self.server_status_lbl = Text(self, padx=5, pady=5, width=20, height=1)
        start_server_btn = Button(self, text="Start Server", command=self.start_server)
        stop_server_btn = Button(self, text="Stop Server", command=self.stop_server)

        start_server_btn.grid(row=0, column=0, padx=5, pady=5, sticky=W + E)
        stop_server_btn.grid(row=0, column=1, padx=5, pady=5, sticky=W + E)
        self.server_status_lbl.grid(row=0, column=2, padx=5, pady=5, sticky=W)

        self.is_init = True
        print("Init completed")

    def centre_window(self):
        sw = self.parent.winfo_screenwidth()
        sh = self.parent.winfo_screenheight()
        x = (sw - WIDTH) >> 1
        y = (sh - HEIGHT) >> 1
        """
        width x height + x + y
        """
        self.parent.geometry('%dx%d+%d+%d' % (WIDTH, HEIGHT, x, y))

    def dispatch_messages(self):
        print("Start messages producer")

        msg = pidata_t()
        msg.timestamp = 0
        msg.front_distance = 0

        while self.is_server_running:
            self.lc.publish(CHANNEL_NAME, msg.encode())
            print(" <- message published")
            time.sleep(1)

        print("Stop messages producer")

    def start_server(self):
        if not self.is_init:
            return False
        if self.is_server_running:
            return False

        self.is_server_running = True

        self.lc = lcm.LCM()
        _thread.start_new_thread(self.dispatch_messages, ())

        self.set_server_status_lbl("Server is running")

    def stop_server(self):
        if not self.is_init:
            return False
        if not self.is_server_running:
            return False

        self.is_server_running = False
        self.set_server_status_lbl("Server is stopped")

    def set_server_status_lbl(self, status_msg):
        print("Server status:" + status_msg)
        self.server_status_lbl.delete("1.0", END)
        self.server_status_lbl.insert(END, status_msg)


def main():
    root = tkinter.Tk()
    App(root)
    root.mainloop()


if __name__ == '__main__':
    main()
